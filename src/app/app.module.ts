import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DragulaModule } from 'ng2-dragula';
import { RouterModule, Routes } from '@angular/router';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgSelectModule } from '@ng-select/ng-select';

import { PrimeNgModule } from './../Module/prime-ng.module';
import { RotasModule } from '../Module/app.routing';

import { HomePage } from '../Paginas/home/home.component';
import { LoginPage } from '../Paginas/login/login.component';
import { HorasPage } from '../Paginas/horas/horas.component';
import { ProjectsPage } from '../Paginas/projects/projects.component';

import { HeaderComponent } from '../Componentes/header/header.component';
import { TasksComponent } from '../Componentes/tasks/tasks.component';
import { MenuComponent } from '../Componentes/menu/menu.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore'
import { AngularFireAuth } from 'angularfire2/auth';

import { environment } from '../environments/environment';
import { DataService } from '../Services/DataSrv/dataSrv.service';



@NgModule({
  declarations: [
    AppComponent,
    HomePage,
    LoginPage,
    HorasPage,
    ProjectsPage,
    HeaderComponent,
    TasksComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    RotasModule,
    DragulaModule,
    BrowserModule,
    PrimeNgModule,
    NgSelectModule,
    BrowserAnimationsModule,
    FormsModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
    MDBBootstrapModule.forRoot(),
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
