import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, Routes } from "@angular/router";
//import { ViewEncapsulation } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  //encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'app';
  display: boolean = false
  AnimateClass: string
  showClass: string = 'displayFalse'
  menubar = []
  constructor(public route: Router) {
    console.log(Math.random().toString(36).substr(2, 9));
  }
  ngOnInit() {

  }
}