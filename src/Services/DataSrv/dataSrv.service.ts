import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import swal from 'sweetalert2';
@Injectable()
export class DataService {
    Item = { lastItem: null, data: null };
    reload: any;
    constructor(public db: AngularFireDatabase) {
        //  this.lastItem('Tarefas')
    }
    lastItem(destino) {
        this.db.list(destino).snapshotChanges()
            .subscribe((item) => {
                this.Item.lastItem = item[item.length - 1];
            });
        return this.Item.lastItem;
    }
    getData(destino) {
      const retorno = [];
      const promise = new Promise((resolve, reject) => {
        this.db.list(destino).valueChanges().subscribe((s) => {
          s.forEach(element => {
            retorno.push(element);
          });
          resolve(retorno);
        });
      });
      return promise;
    }

    update(received) {
        console.log('Olaaaa', received.destino);
        this.db.object(received.destino).update(received.data).then(() => {
            const successMsg = 'Tarefa editada com sucesso';
            this.showToast('top-end', 'success', successMsg, 2000, '#678D65');

        });
    }
    create(received) {
        this.db.object(received.destino).set(received.data).then(() => {
            const successMsg = 'Tarefa criada com sucesso';
            this.showToast('top-end', 'success', successMsg, 2000, '#678D65');

        });
    }
    showToast(position, iconType: any, body: string, time: number, cor: any) {
        swal({
            position: position,
            html: '<h3 style="color:white">' + body + '</h3>',
            showConfirmButton: false,
            background: cor,
            // #678D65
            type: iconType,
            toast: true,
            timer: time,
        });
    }


}
