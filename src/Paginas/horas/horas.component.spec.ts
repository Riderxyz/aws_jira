/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HorasPage } from './Horas.component';

describe('HorasPage', () => {
  let component: HorasPage;
  let fixture: ComponentFixture<HorasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorasPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
