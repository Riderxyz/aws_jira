import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../Services/DataSrv/dataSrv.service';
import { ModalDirective } from 'angular-bootstrap-md';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsPage implements OnInit {
  projetos: any;
  selectedProject: any;
  ProjectSelected = {
    nome: null,
    descricao: null,
    empresaResponsavel: null,
    usuarioResponsavel: null,
    sprint: null,
    id: null
  };
  titleCard: string;
  fireBase = { destino: null, data: null };
  toasterText: any;
  @ViewChild('form') public modal: ModalDirective;
  constructor(public dataSrv: DataService) {
    this.getProject();
  }

  ngOnInit() {
  }

  getProject() {
    this.dataSrv.getData('Projetos').then((projetos) => {
      this.projetos = projetos;
    });
  }
  showDialog() {
    this.ProjectSelected.nome = null;
    this.ProjectSelected.empresaResponsavel = null;
    this.ProjectSelected.usuarioResponsavel = null;
    this.ProjectSelected.sprint = null;
    this.ProjectSelected.descricao = null;
    this.ProjectSelected.id = null;
    this.titleCard = 'Criar nova Projeto';
    this.modal.show();
  }
  selectedTask(select) {
    console.log(select);
    this.titleCard = 'Editando Tarefa ' + select.nome;
    this.ProjectSelected.nome = select.nome;
    this.ProjectSelected.descricao = select.descricao;
    this.ProjectSelected.empresaResponsavel = select.empresaResponsavel;
    this.ProjectSelected.usuarioResponsavel = select.usuarioResponsavel;
    this.ProjectSelected.id = select.id;
    this.modal.show();
  }
  ValidarRegistro() {
    this.toasterText = '';
    if (this.ProjectSelected.nome == null || this.ProjectSelected.nome == '') {
      this.toasterText = this.toasterText + `<div style="
                          padding:10px;
                          color:#fff;
                          font-family: 'Ubuntu', serif;
                          text-align: left;
                          ">
                          <span style="font-weight: bold;">Erro!</span>Você não informou o nome da tarefa</div>`
    }
    if (this.ProjectSelected.empresaResponsavel == null || this.ProjectSelected.empresaResponsavel == '') {
      this.toasterText = this.toasterText + `<div style="
                         padding:10px;
                         color:#fff;
                         font-family: 'Ubuntu', serif;
                         text-align: left;
                         >
                         <span style="font-weight: bold;">Erro!</span>Você não informou a Empresa responsavel por este Projeto</div>`
    }
    return (this.toasterText == '')
  }
  writeProject() {
    console.log(this.ProjectSelected);
    if (this.ProjectSelected.id == null || this.ProjectSelected.id == '') {
      this.createProject()
    } else {
      this.editProject()
    }
  }
  createProject() {
    if (!this.ValidarRegistro()) {
      this.dataSrv.showToast('center', 'error', this.toasterText, 3000, '#B83740')
    } else {
      const idNovo = Math.random().toString(36).substr(2, 9);
      this.fireBase.destino = 'Projetos/' + idNovo;
      this.fireBase.data = {
        nome: this.ProjectSelected.nome,
        descricao: this.ProjectSelected.descricao,
        empresaResponsavel: this.ProjectSelected.empresaResponsavel,
        usuarioResponsavel: this.ProjectSelected.usuarioResponsavel,
        id: idNovo,
      }
      this.dataSrv.create(this.fireBase)
      this.modal.hide()
    };
  }
  editProject() {
    this.fireBase.destino = 'Projetos/' + this.ProjectSelected.id
    this.fireBase.data = {
      nome: this.ProjectSelected.nome,
      descricao: this.ProjectSelected.descricao,
      empresaResponsavel: this.ProjectSelected.empresaResponsavel,
      usuarioResponsavel: this.ProjectSelected.usuarioResponsavel,
    };
    this.dataSrv.update(this.fireBase);
    this.modal.hide();
  }






}
