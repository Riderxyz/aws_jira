import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginPage implements OnInit {
  AnimateClass: string;
  LoginData = { username: null, password: null };
  constructor(public route: Router) {

  }

  ngOnInit() {

  }

  LogIn() {
    this.AnimateClass = 'animated hinge';
    setTimeout(() => {
    this.route.navigateByUrl('Home');
    }, 2500);
  }
}
