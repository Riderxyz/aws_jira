// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBtksv_lGVGD1PxYhVFPKqZrEvGWPnNxIc",
    authDomain: "controledehora-7d96b.firebaseapp.com",
    databaseURL: "https://controledehora-7d96b.firebaseio.com",
    projectId: "controledehora-7d96b",
    storageBucket: "controledehora-7d96b.appspot.com",
    messagingSenderId: "974525748532"
  },

  ConsoleLogs(descricao: string, data: any) {
    console.log(descricao, data);

  }
};
