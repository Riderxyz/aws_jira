
import { NgModule } from "@angular/core";
import { RouterModule, Router, Routes } from "@angular/router";

// Pages

import { LoginPage } from './../Paginas/login/login.component';
import { HomePage } from './../Paginas/home/home.component';
import { ProjectsPage } from "../Paginas/projects/projects.component";
import { HorasPage } from "../Paginas/horas/horas.component";


const AppRoutes: Routes = [
    { path: 'Login', component: LoginPage },
    { path: 'Home', component: HomePage },
    { path: 'Projetos', component: ProjectsPage },
    { path: 'Horas', component: HorasPage },
    { path: '', redirectTo: 'Login', pathMatch: 'full' },

]

@NgModule({
    imports: [
        RouterModule.forRoot(
            AppRoutes, { enableTracing: false }
        )
    ],
    exports: [
        RouterModule
    ]
})



export class RotasModule { }