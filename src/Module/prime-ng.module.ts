import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { ProgressBarModule } from 'primeng/progressbar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { DropdownModule } from 'primeng/dropdown';
import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { ChartModule } from 'primeng/chart';
import { DataGridModule } from 'primeng/datagrid';
import { SlideMenuModule } from 'primeng/slidemenu';
import { PanelMenuModule } from 'primeng/panelmenu';
import { MenuModule } from 'primeng/menu';
import { TreeModule } from 'primeng/tree';
import { FieldsetModule } from 'primeng/fieldset';
import { SidebarModule } from 'primeng/sidebar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { ListboxModule } from 'primeng/listbox';
import { DragDropModule } from 'primeng/dragdrop';
import { EditorModule } from 'primeng/editor';
import { FileUploadModule } from 'primeng/fileupload';
import { SpinnerModule } from 'primeng/spinner';
import { ContextMenuModule } from 'primeng/contextmenu';
import { CalendarModule } from 'primeng/calendar';
@NgModule({
  imports: [
    PanelModule,
    ButtonModule,
    CardModule,
    ToolbarModule,
    ChartModule,
    DataGridModule,
    ProgressBarModule,
    SplitButtonModule,
    DropdownModule,
    SlideMenuModule,
    PanelMenuModule,
    MenuModule,
    TreeModule,
    FieldsetModule,
    SidebarModule,
    ScrollPanelModule,
    DialogModule,
    InputTextModule,
    ListboxModule,
    DragDropModule,
    EditorModule,
    SpinnerModule,
    FileUploadModule,
    ContextMenuModule,
    CalendarModule,
  ],
  declarations: [

  ],
  providers: [

  ],
  exports: [
    PanelModule,
    ButtonModule,
    CardModule,
    ToolbarModule,
    ChartModule,
    FileUploadModule,
    DataGridModule,
    ProgressBarModule,
    SplitButtonModule,
    DropdownModule,
    SlideMenuModule,
    PanelMenuModule,
    MenuModule,
    TreeModule,
    FieldsetModule,
    SidebarModule,
    ScrollPanelModule,
    DialogModule,
    InputTextModule,
    ListboxModule,
    DragDropModule,
    EditorModule,
    SpinnerModule,
    ContextMenuModule,
    CalendarModule,

  ]
})
export class PrimeNgModule { }
