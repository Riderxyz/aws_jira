import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterModule, Router, Routes } from "@angular/router";
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],

})
export class HeaderComponent implements OnInit {
  display: boolean = false;
  AnimateClass: string;
  showClass: string = 'displayFalse';
  menubar = [];
  @ViewChild('fluid') public modal: ModalDirective;
  constructor(public route: Router) {


    console.log(Math.random().toString(36).substr(2, 9));

  }

  ngOnInit() {
    this.menubar =
      [{
        nome: 'Tarefas',
        destino: 'Home',
        class: 'btn btn-default'

      },
      {
        nome: 'Login',
        destino: 'Login',
        class: 'btn btn-default'

      },
      {
        nome: 'Projetos',
        destino: 'Projetos',
        class: 'btn btn-default'

      },
      {
        nome: 'Horas',
        destino: 'Horas',
        class: 'btn btn-default'

      }];
  }
  openSidenav() {
    this.modal.show();
  }

  GoTo(item) {
    console.log(this.route.routerState.snapshot.url);
    this.modal.hide();
    this.route.navigateByUrl(item.destino);
  }
}
