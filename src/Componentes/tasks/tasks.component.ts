
import { Component, OnInit, ViewChild } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { AngularFireDatabase } from 'angularfire2/database';
import { DataService } from '../../Services/DataSrv/dataSrv.service';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  TaskSelected = {
    nome: null,
    descricao: null,
    tempoEstimado: null,
    tempoGasto: null, projeto: null,
    sprint: null,
    situacao: null,
    user: null,
    id: null
  };
  fireBase = { destino: null, data: null };
  Projects: any;
  toasterText: any;
  titleCard: string;
  Bags = { ToDo: [], InProgress: [], Impediment: [], Testing: [], Done: [], BackLog: [] };
  @ViewChild('form') public modal: ModalDirective;
  constructor(public dragService: DragulaService, public db: AngularFireDatabase, public dataSrv: DataService) {
    dragService.setOptions('another-bag', {
      revertOnSpill: true
    });
    this.dataSrv.lastItem('Tarefas');
    this.dragService.dropModel.subscribe((value) => {
      this.updateSituation();
    });
  }

  updateSituation() {
    for (let i = 0; i < this.Bags.ToDo.length; i++) {
      const element = this.Bags.ToDo[i];
      if (element.situacao != 'To Do') {
        element.situacao = 'To Do';
        this.fireBase.data = { situacao: element.situacao };
        this.fireBase.destino = 'Tarefas/' + element.id;
        this.dataSrv.update(this.fireBase);
      }
    }
    for (let i = 0; i < this.Bags.InProgress.length; i++) {
      const element = this.Bags.InProgress[i];
      if (element.situacao != 'In Progress') {
        element.situacao = 'In Progress';
        console.log(element.id);
        this.fireBase.data = { situacao: element.situacao };
        this.fireBase.destino = 'Tarefas/' + element.id;
        this.dataSrv.update(this.fireBase);
      }
    }
    for (let i = 0; i < this.Bags.Testing.length; i++) {
      const element = this.Bags.Testing[i];
      if (element.situacao != 'Testing') {
        element.situacao = 'Testing'
        this.fireBase.data = { situacao: element.situacao };
        this.fireBase.destino = 'Tarefas/' + element.id;
        this.dataSrv.update(this.fireBase);
      }
    }
    for (let i = 0; i < this.Bags.Done.length; i++) {
      const element = this.Bags.Done[i];
      if (element.situacao != 'Done') {
        element.situacao = 'Done'
        this.fireBase.data = { situacao: element.situacao };
        this.fireBase.destino = 'Tarefas/' + element.id;
        this.dataSrv.update(this.fireBase);
      }
    }
    for (let i = 0; i < this.Bags.BackLog.length; i++) {
      const element = this.Bags.BackLog[i];
      if (element.situacao != 'BackLog') {
        element.situacao = 'BackLog'
        this.fireBase.data = { situacao: element.situacao };
        this.fireBase.destino = 'Tarefas/' + element.id;
        this.dataSrv.update(this.fireBase);
      }
    }
    for (let i = 0; i < this.Bags.Impediment.length; i++) {
      const element = this.Bags.Impediment[i];
      if (element.situacao != 'Impediment') {
        element.situacao = 'Impediment';
        this.fireBase.data = { situacao: element.situacao };
        this.fireBase.destino = 'Tarefas/' + element.id;
        this.dataSrv.update(this.fireBase);
      }
    }
  }
  ngOnDestroy(): void {
    this.dragService.destroy('another-bag');
  }
  ngOnInit() {
    this.getTask();
  }
  getTask() {
    this.dataSrv.getData('Tarefas').then((tarefas) => {
        this.Bags.Done = [];
        this.Bags.Testing = [];
        this.Bags.InProgress = [];
        this.Bags.BackLog = [];
        this.Bags.Impediment = [];
        this.Bags.ToDo = [];
        this.extractdata(tarefas);
      });
      this.dataSrv.getData('Projetos').then((projeto) => {
        this.Projects = projeto;
      });

  }
  extractdata(item) {
    for (let i = 0; i < item.length; i++) {
      const element = item[i];
      if (element.situacao === 'Done') {

        this.Bags.Done.push(element);
      }
      if (element.situacao === 'Testing') {

        this.Bags.Testing.push(element);
      }
      if (element.situacao === 'In Progress') {

        this.Bags.InProgress.push(element);
      }
      if (element.situacao === 'BackLog') {

        this.Bags.BackLog.push(element);
      }
      if (element.situacao === 'Impediment') {

        this.Bags.Impediment.push(element);
      }
      if (element.situacao === 'To Do') {

        this.Bags.ToDo.push(element);
      }
    }
  }
  Onchange(type) {
    this.TaskSelected.projeto = type.value;
  }
  showDialog() {
    this.TaskSelected.nome = null;
    this.TaskSelected.tempoEstimado = null;
    this.TaskSelected.tempoGasto = null;
    this.TaskSelected.user = null;
    this.TaskSelected.projeto = null;
    this.TaskSelected.sprint = null;
    this.TaskSelected.descricao = null;
    this.TaskSelected.id = null;
    this.titleCard = 'Criar nova Tarefa';
    this.modal.show();
    console.log('Funcionou "-" ');

  }
  selectedTask(select) {
    console.log(select);
    this.titleCard = 'Editando Tarefa ' + select.nome;
    this.TaskSelected.nome = select.nome;
    this.TaskSelected.descricao = select.descricao;
    this.TaskSelected.projeto = select.projeto;
    this.TaskSelected.tempoEstimado = select.horasEstimadas;
    this.TaskSelected.tempoGasto = select.horasGastas;
    this.TaskSelected.situacao = select.situacao;
    this.TaskSelected.id = select.id;
    this.modal.show();
  }
  ValidarRegistro() {
    this.toasterText = '';
    if (this.TaskSelected.nome == null || this.TaskSelected.nome == '') {
      this.toasterText = this.toasterText + `<div style="
                          padding:10px;
                          color:#fff;
                          font-family: 'Ubuntu', serif;
                          text-align: left;
                          ">
                          <span style="font-weight: bold;">Erro!</span>Você não informou o nome da tarefa</div>`
    }
    if (this.TaskSelected.projeto == null || this.TaskSelected.projeto == '') {
      this.toasterText = this.toasterText + `<div style="
                         padding:10px;
                         color:#fff;
                         font-family: 'Ubuntu', serif;
                         text-align: left;
                         >
                         <span style="font-weight: bold;">Erro!</span>Você não informou a qual projeto essa tarefa pertence</div>`
    }
    if (this.TaskSelected.tempoEstimado == null || this.TaskSelected.tempoEstimado == '') {
      this.toasterText = this.toasterText + `<div style="
                         padding:10px;
                         color:#fff;
                         font-family: 'Ubuntu', serif;
                         text-align: left;
                         >
                         <span style="font-weight: bold;">Erro!</span>Você não o tempo estimado desta tarefa</div>`
    }
    return (this.toasterText == '');
  }
  Task() {
    console.log(this.TaskSelected);
    if (this.TaskSelected.id === null || this.TaskSelected.id === '') {
      this.createTask();
      console.log(this.TaskSelected.projeto);
    } else {
      this.editTask();
      console.log(this.TaskSelected.projeto);
    }
  }
  createTask() {
    if (!this.ValidarRegistro()) {
      this.dataSrv.showToast('center', 'error', this.toasterText, 3000, '#B83740');
      console.log('hell', this.TaskSelected.projeto);
    } else {
      const idNovo = Math.random().toString(36).substr(2, 9);
      this.fireBase.destino = 'Tarefas/' + idNovo;
      this.fireBase.data = {
        nome: this.TaskSelected.nome,
        descricao: this.TaskSelected.descricao,
        projeto: this.TaskSelected.projeto,
        horasEstimadas: this.TaskSelected.tempoEstimado,
        horasGastas: this.TaskSelected.tempoGasto,
        situacao: 'To Do',
        id: idNovo,
      };
      this.dataSrv.create(this.fireBase);
      this.modal.hide();
      this.getTask();
    }
  }
  editTask() {
    this.fireBase.destino = 'Tarefas/' + this.TaskSelected.id;
    this.fireBase.data = {
      nome: this.TaskSelected.nome,
      descricao: this.TaskSelected.descricao,
      projeto: this.TaskSelected.projeto,
      horasEstimadas: this.TaskSelected.tempoEstimado,
      horasGastas: this.TaskSelected.tempoGasto,
      situacao: this.TaskSelected.situacao,
    };
    this.dataSrv.update(this.fireBase);
    this.modal.hide();
    this.getTask();
  }
}
